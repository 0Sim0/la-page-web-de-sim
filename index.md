---
title: "Accueil"
order: 0
in_menu: true
---
![bandeau - préhistorique]({% link images/Image-0001.jpg %})

## La page WEB de Sim

Ce site est mon blog personnel. Je vais y déposer quelques illustrations faites de mes mains :-) . La licence les concernant est la suivante :

 La page Web de Sim © 2024 by Sim is licensed under CC BY-SA 4.0  

Je vais aussi évoquer mes différents centres d'intérêt que sont : le Cinéma, tout ce qui est lié au dessin comme les ~~romans graphiques~~, je veux dire la Bande Dessinée dans son ensemble, que ce soit Comics, Manga, Franco-Belge ou Belgo-française (c'est comme on veut !). Mais aussi pourquoi ne pas parler de logiciels libres…

Je ne suis pas fort en "communication". Je pensais que créer m'aiderais. Je verrai bien, car cela ne fait qu'environ 5 années que je dessine par exemple. Je me ferai un plaisir d'observer mon évolution sur [Scribouilli](https://scribouilli.org/)

Je profite ainsi de la création de Scribouilli (vu sur [Linuxfr.org](https://linuxfr.org/news/scribouilli-v1-0-un-outil-simple-pour-des-petits-sites-et-des-blogs)). Je remercie le groupe d'ami-es à l'origine de ce projet).

À bientôt !

# Sim 

![portrait cerf anthropomorphe]({% link images/IMG_20240123_175732_1.jpg %})

![Peace]({% link images/IMG_20240123_180029.jpg %}) 

N.B. : Ce blog fait partie de mon monde, ainsi, il peut paraître étrange. J'espère malgré tout que cela vous convaincra !  :-) 